# Cucumdroid annotations library #
This library provides classes for annotating code to be executed by the Cucumdroid library. It also provides a base class for test classes so that they do not to import the uiAutomator.jar.

### This library requires ###
The uiautomator.jar from the Android SDK

JRE 1.6


----------
### License ###
This work is licensed under a [Creative Commons Attribution-ShareAlike 3.0 Unported License](http://creativecommons.org/licenses/by-sa/3.0/deed.en_GB "details").

