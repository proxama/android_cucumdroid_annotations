/**
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License.
 * http://creativecommons.org/licenses/by-sa/3.0/deed.en_GB
**/
package com.proxama.android.cucumdroid.testcase;

import com.android.uiautomator.core.UiDevice;

public abstract class CucumdroidTestCase {

	private UiDevice mDevice;
	public void setUiDevice(final UiDevice device) {
		mDevice = device;
	}
	
	public UiDevice getUiDevice() {
		return mDevice;
	}
}
